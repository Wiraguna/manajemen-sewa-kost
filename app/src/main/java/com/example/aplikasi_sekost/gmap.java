package com.example.aplikasi_sekost;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import androidx.fragment.app.FragmentManager;

import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

public class gmap extends AppCompatActivity {
    Button gmaps;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_home);

        gmaps = findViewById(R.id.gmap1);
        gmaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoUrl("https://www.youtube.com/watch?v=Bx_qsu-kQ2U");
                //gotoUrl( s: "https://www.google.co.id/maps/place/Gg.+Merdeka+I+No.45,+Dasan+Agung+Baru,+Kec.+Selaparang,+Kota+Mataram,+Nusa+Tenggara+Bar.+83113/@-8.5782119,116.0909545,17z/data=!3m1!4b1!4m5!3m4!1s0x2dcdc0862b098e53:0x7188a7a9e6f3cbed!8m2!3d-8.5782119!4d116.0909545");
            }
        });
    }
    private void gotoUrl(String s) {
            Uri uri = Uri.parse(s);
            startActivity(new Intent(Intent.ACTION_VIEW,uri));
    }
}
