package com.example.aplikasi_sekost.fragmentMenu;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.aplikasi_sekost.BantuanActivity;
import com.example.aplikasi_sekost.Detail_pembayaran_Activity;
import com.example.aplikasi_sekost.Ketetapan_Akun_Activity;
import com.example.aplikasi_sekost.R;
import com.example.aplikasi_sekost.Riwayat_Pembayaran_Activity;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PembayaranFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PembayaranFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Button button;

    public PembayaranFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PembayaranFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PembayaranFragment newInstance(String param1, String param2) {
        PembayaranFragment fragment = new PembayaranFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View  rooview = inflater.inflate(R.layout.fragment_pembayaran, container, false);

        Button btn =(Button) rooview.findViewById(R.id.pbyrn_detail);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), Detail_pembayaran_Activity.class);
                startActivity(intent);
            }
        });


        Button btn1 =(Button) rooview.findViewById(R.id.pbyrn_konfirmasi);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), Riwayat_Pembayaran_Activity.class);
                startActivity(intent);
            }
        });

        return rooview;
    }
}