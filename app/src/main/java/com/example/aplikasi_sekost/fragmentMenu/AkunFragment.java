package com.example.aplikasi_sekost.fragmentMenu;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.aplikasi_sekost.Akun_tentang_aplikasiActivity;
import com.example.aplikasi_sekost.BantuanActivity;
import com.example.aplikasi_sekost.Ketetapan_Akun_Activity;
import com.example.aplikasi_sekost.R;

import org.jetbrains.annotations.NotNull;

public class AkunFragment extends Fragment {

    Button button;


    public AkunFragment(){

    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootview   =   inflater.inflate(R.layout.fragment_akun, container, false);

        Button btn =(Button) rootview.findViewById(R.id.akun_bantuan1);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), BantuanActivity.class);
                startActivity(intent);
            }
        });


        Button btn1 =(Button) rootview.findViewById(R.id.akun_ketentuanlayanan1);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), Ketetapan_Akun_Activity.class);
                startActivity(intent);
            }
        });

        Button btn2 =(Button) rootview.findViewById(R.id.akun_tentangapps1);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), Akun_tentang_aplikasiActivity.class);
                startActivity(intent);
            }
        });


        return  rootview;
    }


}