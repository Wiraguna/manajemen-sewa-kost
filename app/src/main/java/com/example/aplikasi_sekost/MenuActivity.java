package com.example.aplikasi_sekost;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

import com.example.aplikasi_sekost.fragmentMenu.AkunFragment;
import com.example.aplikasi_sekost.fragmentMenu.HomeFragment;
import com.example.aplikasi_sekost.fragmentMenu.PembayaranFragment;
import com.example.aplikasi_sekost.fragmentMenu.PenyewaFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.jetbrains.annotations.NotNull;

public class MenuActivity extends AppCompatActivity {
    private BottomNavigationView bottomNavigationView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_menu2);


        bottomNavigationView = findViewById(R.id.bottomNavigasi);

        bottomNavigationView.setOnNavigationItemSelectedListener(bottomNavMethod);
        getSupportFragmentManager().beginTransaction().replace(R.id.container,new HomeFragment()).commit();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener bottomNavMethod= menuItem -> {
        Fragment fragment = null;
        switch (menuItem.getItemId())
        {
            case R.id.home:
                fragment = new HomeFragment();
                break;

            case R.id.penyewa:
                fragment = new PenyewaFragment();
                break;

            case R.id.pembayaran:
                fragment = new PembayaranFragment();
                break;
            case R.id.akun:
                fragment = new AkunFragment();
                break;
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.container,fragment).commit();





        return true;
    };
}