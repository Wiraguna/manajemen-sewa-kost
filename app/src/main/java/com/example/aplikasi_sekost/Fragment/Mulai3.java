package com.example.aplikasi_sekost.Fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.aplikasi_sekost.Menu_Activity;
import com.example.aplikasi_sekost.R;

/*
 * A simple {@link Fragment} subclass.
 * Use the {@link Mulai3#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Mulai3 extends Fragment {

   /* // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;*/

    public Mulai3() {
        // Required empty public constructor
    }

    /*
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Mulai3.
     */
    // TODO: Rename and change types and number of parameters
    /*public static Mulai3 newInstance(String param1, String param2) {
        Mulai3 fragment = new Mulai3();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }*/

    //@Override
    /*public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_mulai3, container, false);

       /* Button muali3 = (Button) rootView.findViewById(R.id.mulai3);
        muali3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =  new Intent(getActivity(), Menu_Activity.class);
                startActivity(intent);

            }
        });

        return rootView;*/


        Button bt1 =view.findViewById(R.id.mulai3);
        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), Menu_Activity.class);
                startActivity(intent);
            }
        });
        return view;

    }


}