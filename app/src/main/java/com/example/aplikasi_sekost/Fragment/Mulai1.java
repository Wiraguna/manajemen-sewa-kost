package com.example.aplikasi_sekost.Fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.aplikasi_sekost.Menu_Activity;
import com.example.aplikasi_sekost.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Mulai1#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class Mulai1 extends Fragment {

    Button button;

   /* // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    *//**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Mulai1.
     *//*
    // TODO: Rename and change types and number of parameters
    public static Mulai1 newInstance(String param1, String param2) {
        Mulai1 fragment = new Mulai1();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }*/

    public Mulai1() {
        // Required empty public constructor
    }

   /* @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

          View rootview   =   inflater.inflate(R.layout.fragment_mulai1, container, false);

          Button btn =(Button) rootview.findViewById(R.id.button2);
          btn.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                  Intent intent = new Intent(getActivity(), Menu_Activity.class);
                  startActivity(intent);
              }
          });
        return  rootview;
    }

    @Override
    public void onViewCreated(@NonNull @org.jetbrains.annotations.NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        button = view.findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                Mulai2 mulai2 = new Mulai2();
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.Fragmen_container, mulai2, Mulai2.class.getSimpleName())
                        .commit();
            }
        });
    }
}



